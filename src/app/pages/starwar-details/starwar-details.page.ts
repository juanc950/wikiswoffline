import { Component, OnInit } from '@angular/core';
import { ApiService, SearchType } from './../../services/api.service';
import { ActivatedRoute } from '@angular/router';
import { DatabaseService, fil, peo, pla } from './../../services/database.service';

@Component({
  selector: 'app-starwar-details',
  templateUrl: './starwar-details.page.html',
  styleUrls: ['./starwar-details.page.scss'],
})
export class StarwarDetailsPage implements OnInit {

  information = null;
  constructor(
    private activatedRoute: ActivatedRoute, 
    private apiserv: ApiService,
    private db: DatabaseService) { }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');

    this.apiserv.GetDetallSwapi(id).subscribe(result => {
      this.information = result;
    });
  }

}

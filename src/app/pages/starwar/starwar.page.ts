import { Component, OnInit } from '@angular/core';
import { Platform, AlertController } from '@ionic/angular';
import { ApiService, SearchType } from './../../services/api.service';
import { Observable, from, BehaviorSubject } from 'rxjs';
import { DatabaseService, fil, peo, pla } from './../../services/database.service';
import { ToastController, MenuController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { NetworkService, ConnectionStatus } from './../../services/network.service';
import { OfflineManagerService } from './../../services/offline-manager.service';

var pelicula: string = 'films';
var personaje: string = 'people';
var planeta: string = 'planets';
let Verifica: boolean = true;
@Component({
  selector: 'app-starwar',
  templateUrl: './starwar.page.html',
  styleUrls: ['./starwar.page.scss'],
})

export class StarwarPage implements OnInit {
  pelicul: fil[] = [];
  persone: peo[] = [];
  planete: pla[] = [];
  //films: Observable<any>;
  peli: any;
  films: any;
  type: SearchType = SearchType.films;// asemos llamado a la lista searshtype de la api y valorizamos el dato que traemos en la seleccion

  pelic: fil = null;
  peopl: peo = null;
  plant: pla = null;

  constructor(public apiService: ApiService, private plt: Platform,
    private db: DatabaseService,
    private toastController: ToastController,
    private http: HttpClient,
    private offlineManager: OfflineManagerService,
    private networkService: NetworkService,
    public alerta: AlertController,
    private menu: MenuController
  ) { }


  ngOnInit() {
    this.plt.ready().then(() => {
      this.CargList();
    });
  }

  cargardatosoffline() {
    this.presenAlert();
    this.db.deletfilm();
  }

  CargList() {
    if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline) {
      this.cargarlistaSQL();
    }
    if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
      //this.films = this.apiService.GetSwapi(this.type);
      this.cargarlistaSQL()//
    }
  }
  cargarlistaSQL() {


    if (this.type == pelicula) {
      this.db.loadfilms().then((data)=> {
        console.log(data);
        this.films = data;
      })
    }



    if (this.type == personaje) {
      this.db.getDatabasesState().subscribe(rdy => {
        if (rdy) {
          this.db.getpeoples().subscribe(devs => {
            this.persone = devs;
          })
        }
      })
    }
    if (this.type == planeta) {
      this.db.getDatabasesState().subscribe(rdy => {
        if (rdy) {
          this.db.getplanets().subscribe(devs => {
            this.planete = devs;
          })
        }
      })
    }
  }

  async presenAlert() {
    const alert = await this.alerta.create({
      header: 'Alert',
      subHeader: 'Cargando datos',
      message: 'guardando los datos localmente',
      buttons: ['ok']
    });
    await alert.present();
  }
}

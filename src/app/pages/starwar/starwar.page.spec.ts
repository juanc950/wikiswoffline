import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StarwarPage } from './starwar.page';

describe('StarwarPage', () => {
  let component: StarwarPage;
  let fixture: ComponentFixture<StarwarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StarwarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StarwarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

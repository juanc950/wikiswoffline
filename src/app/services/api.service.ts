import { Injectable } from '@angular/core';
import { DatabaseService } from './../services/database.service';
import { HttpClient } from '@angular/common/http';
import { NetworkService, ConnectionStatus } from './network.service';
import { Storage } from '@ionic/storage';
import { ToastController } from '@ionic/angular';

const API_STORAGE_KEY = 'specialkey';
const API_URL = 'https://swapi.co/api';
const API_URL1 = 'https://swapi.co/api/films/?format=json';
const API_URL2 = 'https://swapi.co/api/planets/?format=json';
const API_URL3 = 'https://swapi.co/api/people/?format=json';
export enum SearchType {// lista para especificar el serachtype que le mandamos del pagin a la api
  films = 'Peliculas',
  people = 'Personajes',
  planets = 'Planetas'
}
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http: HttpClient, private networkService: NetworkService, private storage: Storage,
    private toastController: ToastController, private db: DatabaseService
  ) { }
  GetSwapi(type: SearchType) {
    if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline)// pedimos el tipo el resultado de coneccion y lo igualamos si es sin hay servicio o no
    {
      return this.http.get(`CONECCION FAIDA`);
    } else {
      return this.http.get(`${API_URL}/${type}`);
          
    }
  }
  obtenerDatosfilm() {
    return this.http.get(`${API_URL1}`);
  }
  obtenerDatosplanet() {
    return this.http.get(`${API_URL2}`);
  }
  obtenerDatosPeople() {
    return this.http.get(`${API_URL3}`);
  }
  GetDetallSwapi(id) {
    if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline)// pedimos el tipo el resultado de coneccion y lo igualamos si es sin hay servicio o no
    {
      return this.http.get(`CONECCION FAIDA`);
    } else {
      //return this.http.get(`${API_URL}/films/${id}/`);
      return this.http.get(`${id}`);
    }
  }

}
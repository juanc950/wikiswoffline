import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { DatabaseService, fil, peo, pla } from './../services/database.service';
import { ApiService, SearchType } from './../services/api.service';
import { Platform, AlertController } from '@ionic/angular';
import { NetworkService, ConnectionStatus } from './../services/network.service';
import { Observable, from, BehaviorSubject } from 'rxjs';

var pelicula: string = 'films';
var personaje: string = 'people';
var planeta: string = 'planets';
let Verifica: boolean = false;
@Injectable({
  providedIn: 'root'
})
export class OfflineManagerService {
  people;
  planet;
  film;
  constructor(private apiService: ApiService, private plt: Platform,
    private db: DatabaseService,
    private toastController: ToastController,
    private http: HttpClient,
    private networkService: NetworkService,
    public alerta: AlertController) { }

  CargListDatabaseSQL() {
    if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Online) {
      this.apiService.obtenerDatosfilm()
        .subscribe(
          (data) => { this.film = data; },
          (error) => console.log(error)
        )

      this.apiService.obtenerDatosplanet()
        .subscribe(
          (data) => { this.planet = data; },
          (error) => console.log(error)
        )

      this.apiService.obtenerDatosPeople()
        .subscribe(
          (data) => { this.people = data; },
          (error) => console.log(error)
        )
      
      console.log(this.planet);
      console.log(this.people);
      console.log(this.film);


      this.llenarDatabaseFilm();
      this.llenarDatabasePlanet();
      this.llenarDatabasePeople();
    }
  }

  llenarDatabaseFilm() {
    for (let i = 0; i < this.film.results.length; i++) {
      this.db.addfilms(i, this.film.results[i].title, this.film.results[i].episode_id, this.film.results[i].opening_crawl, this.film.results[i].release_date);
    }
  }
  llenarDatabasePlanet() {
    for (let i = 0; i < this.planet.results.length; i++) {
      this.db.addplanets(i, this.planet.results[i].name, this.planet.results[i].diameter, this.planet.results[i].population);
    }
  }
  llenarDatabasePeople() {
    for (let i = 0; i < this.people.results.length; i++) {
      this.db.addpeoples(i, this.people.results[i].name, this.people.results[i].height, this.people.results[i].mass, this.people.results[i].gender);
    }
  }

  EliminarDatosCompletos() {
    this.db.deletfilm();
    this.db.deletpeoples();
    this.db.deletplanets();
  }

}

import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { HttpClient } from '@angular/common/http';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { BehaviorSubject, Observable } from 'rxjs';
import { StarwarPage } from '../pages/starwar/starwar.page';


// declaramos los campos a utilizar
export interface fil {
  id: number,
  title: string,
  episode_id: string,
  opening_crawl: string,
  release_date: string
}

export interface peo {
  id: number,
  names: string,
  height: string,
  mass: string,
  gender: string
}

export interface pla {
  id: number,
  names: string,
  diameter: string,
  populations: string
}

@Injectable({
  providedIn: 'root'
})

export class DatabaseService {

  private db: SQLiteObject;
  private dbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

  // creamos un areglo donde contendremos los datos obtenidos
  filmes = new BehaviorSubject([]);
  peoples = new BehaviorSubject([]);
  planets = new BehaviorSubject([]);

  // en el constructor ejecutamos la centencia para generar la base de datos 
  constructor(private plt: Platform, private sqlitePorter: SQLitePorter, private sqlite: SQLite, private http: HttpClient) {
    this.plt.ready().then(() => {
      this.sqlite.create({
        name: 'starwarswiki.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.db = db;
          this.seedDatabase();
        });
    });
  }
  // le decimos en donde tomar los datos para crear las tablas y cargar las tablas
  seedDatabase() {
    this.http.get('assets/seed.sql', { responseType: 'text' })
      .subscribe(sql => {
        this.sqlitePorter.importSqlToDb(this.db, sql)
          .then(_ => {
            this.loadfilms(); // cargamos la tabla pelicula del SQL select
            this.loadpeoples(); // cargamos la tabla personajes
            this.loadplanets(); // carmagos la tabla planetas
            this.dbReady.next(true);
          })
          .catch(e => console.error(e));
      });
  }
  // en esta parte obtenemos el resultado de la carga y lo confirma
  getDatabasesState() {
    return this.dbReady.asObservable();
  }
  //--------------------------------------------------------- sentencia SQL para la tabla Pelicula -------------------------
  // obtenemos los resultados de la carga de la tabla en el areglo
  getfilmes(): Observable<any[]> {
    return this.filmes.asObservable();
  }
  // registramos datos en la tabla 
  addfilms(id, title, episode_id, opening_crawl, release_date) {
    let data = [id, title, episode_id, opening_crawl, release_date];
    //console.log(data);
    let sql = "INSERT or IGNORE INTO pelicula (id, title, episode_id, opening_crawl, release_date) VALUES (?,?,?,?,?)";
    return this.db.executeSql(sql, data).then((data) => {
      this.loadfilms();
    });
  }
  deletfilm() {
    let sql = "DELETE FROM pelicula"
    return this.db.executeSql(sql).then((data) => {
      this.loadfilms();
    });
  }
  // cargamos los datos obtenidos de la tabla a un areglo
  /*loadfilms(){
    return this.db.executeSql('SELECT * FROM pelicula', []).then(data => {
      let filmes = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          filmes.push({
            id: data.rows.item(i).id,
            title: data.rows.item(i).title,
            episode_id: data.rows.item(i).episode_id,
            opening_crawl: data.rows.item(i).opening_crawl,
            release_date: data.rows.item(i).release_date
          });
        }
      }
      this.filmes.next(filmes);
    });
  }*/

  loadfilms() {
    return new Promise((resolve, reject) => {
      this.db.executeSql('SELECT * FROM pelicula', []).then(data => {
        let arrayFilmes = [];
        if (data.rows.length > 0) {
          for (var i = 0; i < data.rows.length; i++) {
            arrayFilmes.push({
              id: data.rows.item(i).id,
              title: data.rows.item(i).title,
              episode_id: data.rows.item(i).episode_id,
              opening_crawl: data.rows.item(i).opening_crawl,
              release_date: data.rows.item(i).release_date
            });
          }
        }
        resolve(arrayFilmes);
      }, (error)=> {
        reject(error);
      })
    })
  }
  //--------------------------------------------------------- sentencia SQL para la tabla Personajes -------------------------

  getpeoples(): Observable<peo[]> {
    return this.peoples.asObservable();
  }

  addpeoples(id, names, height, mass, gender) {
    let data = [id, names, height, mass, gender];
    //console.log(data);
    let sql = "INSERT or IGNORE INTO personajes (id, names, height,  mass, gender)VALUES (?,?,?,?,?)";
    return this.db.executeSql(sql, data).then((data) => {
      this.loadpeoples();
    });
  }

  deletpeoples() {
    let sql = "DELETE FROM personajes"
    return this.db.executeSql(sql).then((data) => {
      this.loadfilms();
    });
  }

  loadpeoples() {
    return this.db.executeSql('SELECT * FROM personajes', []).then(data => {
      let peoples: peo[] = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          peoples.push({
            id: data.rows.item(i).id,
            names: data.rows.item(i).names,
            height: data.rows.item(i).height,
            mass: data.rows.item(i).mass,
            gender: data.rows.item(i).gender
          });
        }
      }
      this.peoples.next(peoples);
    });
  }

  //--------------------------------------------------------- sentencia SQL para la tabla Planetas ---------------------------

  getplanets(): Observable<pla[]> {
    return this.planets.asObservable();
  }

  addplanets(id, names, diameter, populations) {
    let data = [id, names, diameter, populations];
    //console.log(data);
    let sql = "INSERT or IGNORE INTO planetas (id, names, diameter, populations) VALUES (?,?,?,?)";
    return this.db.executeSql(sql, data).then((data) => {
      this.loadplanets();
    });
  }

  deletplanets() {
    let sql = "DELETE FROM planetas"
    return this.db.executeSql(sql).then((data) => {
      this.loadfilms();
    });
  }

  loadplanets() {
    return this.db.executeSql('SELECT * FROM planetas', []).then(data => {
      let planets: pla[] = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          planets.push({
            id: data.rows.item(i).id,
            names: data.rows.item(i).names,
            diameter: data.rows.item(i).diameter,
            populations: data.rows.item(i).populations
          });
        }
      }
      this.planets.next(planets);
    });
  }

}

import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
const routes: Routes = [
  { path: '', redirectTo: 'starwar', pathMatch: 'full' },
  //{ path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'starwar', loadChildren: './pages/starwar/starwar.module#StarwarPageModule' },
  { path: 'starwar/:id', loadChildren: './pages/starwar-details/starwar-details.module#StarwarDetailsPageModule' },
 // { path: 'starwar-details/:id', loadChildren: './pages/starwar-details/starwar-details.module#StarwarDetailsPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

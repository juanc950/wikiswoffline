CREATE TABLE IF NOT EXISTS pelicula(id INTEGER PRIMARY KEY,title TEXT,episode_id INTEGER,opening_crawl TEXT,release_date TEXT);
CREATE TABLE IF NOT EXISTS personajes(id INTEGER PRIMARY KEY,names TEXT,height INTEGER, mass INTEGER, gender TEXT);
CREATE TABLE IF NOT EXISTS planetas(id INTEGER PRIMARY KEY, names TEXT,diameter INTEGER,populations INTEGER);